<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Cache;

class RatesController extends Controller
{
    private const CBR_CODES_URL = 'http://www.cbr.ru/scripts/XML_valFull.asp';
    private const CBR_RATES_URL = 'http://www.cbr.ru/scripts/XML_dynamic.asp';

    private const ROUND_PRECISION = 4;

    public function index(string $currency, ?string $date = null, string $baseCurrency = 'RUR'): JsonResponse
    {
        try {
            return response()->json($this->getRates($currency, $date, $baseCurrency));
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Получаем курс валюты на указанную дату + разницу с предыдущем днем - В БАЗОВОЙ ВАЛЮТЕ
     *
     * @param string $currency
     * @param string|null $date
     * @param string $baseCurrency
     * @return array
     * @throws Exception
     */
    private function getRates(string $currency, ?string $date, string $baseCurrency): array
    {
        if (is_null($date)) {
            $date = date('Y-m-d');
        }

        if (($date = Carbon::createFromFormat('Y-m-d', $date)) === false) {
            throw new Exception('Ошибка. Дата указана в некорректном формате. Укажите дату в формате ГГГГ-ММ-ДД');
        }

        if ($date > Carbon::now()) {
            throw new Exception('Ошибка. Указана дата из будущего');
        }

        $currencyRateOnDate = $this->getCurrencyRateOnDate($currency, $date);

        if ($baseCurrency !== 'RUR') {
            $baseCurrencyRateOnDate = $this->getCurrencyRateOnDate($baseCurrency, $date);

            $rate = $currencyRateOnDate['current'] / $baseCurrencyRateOnDate['current'];
            $diff = $rate - $currencyRateOnDate['before'] / $baseCurrencyRateOnDate['before'];

            $result = [
                'rate' => $rate,
                'diff' => $diff
            ];
        } else {
            $result = [
                'rate' => $currencyRateOnDate['current'],
                'diff' => $currencyRateOnDate['current'] - $currencyRateOnDate['before']
            ];
        }

        return array_map(function ($v) { return round($v, self::ROUND_PRECISION); }, $result);
    }

    /**
     * Получаем курс валюты на указанную дату + разницу с предыдущем днем - В РУБЛЯХ
     *
     * @param string $currency
     * @param Carbon $date
     * @return array
     * @throws Exception
     */
    private function getCurrencyRateOnDate(string $currency, Carbon $date): array
    {
        try {

            $currencyCbrCode = $this->convertIsoToCbr($currency);

            $cacheKey = 'currency-rates-' . $currencyCbrCode . '-' . $date->format('Y-m-d');

            $currencyRateOnDate = Cache::get($cacheKey);

            if (is_null($currencyRateOnDate)) {

                $xmlUrl = self::CBR_RATES_URL . '?' . http_build_query([
                    'date_req1' => $date->copy()->subDays(1)->format('d/m/Y'),
                    'date_req2' => $date->format('d/m/Y'),
                    'VAL_NM_RQ' => $currencyCbrCode
                ]);

                $xml = simplexml_load_string(file_get_contents($xmlUrl));


                $firstRate = str_replace(',', '.', (string) $xml->Record[0]->Value);
                $firstNominal = str_replace(',', '.', (string) $xml->Record[0]->Nominal);

                $secondRate = str_replace(',', '.', (string) $xml->Record[1]->Value);
                $secondNominal = str_replace(',', '.', (string) $xml->Record[1]->Nominal);

                $currencyRateOnDate = [
                    'before' => $firstRate / $firstNominal,
                    'current' => $secondRate / $secondNominal,
                ];

                Cache::put($cacheKey, $currencyRateOnDate);
            }

            return $currencyRateOnDate;
        } catch (Exception $e) {
            throw new Exception('Ошибка. Не удалось получить данные из ЦБ');
        }
    }

    /**
     * Конвертируем ISO код в ЦБР код
     *
     * @param string $currencyCode
     * @return string
     * @throws Exception
     */
    private function convertIsoToCbr(string $currencyCode): string
    {
        $cbrCurrencyCodes = $this->getCbrCurrenciesCodes();

        if (!array_key_exists($currencyCode, $cbrCurrencyCodes)) {
            throw new Exception('Ошибка. Указан неверный код валюты');
        }

        return $cbrCurrencyCodes[$currencyCode];
    }

    /**
     * Загружаем коды валют ЦБР для конвертации ISO кодов в коды ЦБР
     *
     * @return array
     * @throws Exception
     */
    private function getCbrCurrenciesCodes(): array
    {
        try {
            $cbrCurrencyCodes = Cache::get('currency-codes');

            if (is_null($cbrCurrencyCodes)) {

                $cbrCurrencyCodes = [];

                $xml = simplexml_load_string(file_get_contents(self::CBR_CODES_URL));
                foreach ($xml->Item as $item) {

                    $ISOCode = (string) $item->ISO_Char_Code;
                    $CBRCode = (string) $item->attributes()->ID;

                    if (!empty($ISOCode) && !empty($CBRCode)) {
                        $cbrCurrencyCodes[$ISOCode] = $CBRCode;
                    }
                }

                Cache::put('currency-codes', $cbrCurrencyCodes);
            }

            return $cbrCurrencyCodes;
        } catch (Exception $e) {
            throw new Exception('Ошибка. Не удалось получить данные из ЦБ');
        }
    }
}
